export const App = () => {
  return (
    <main className="min-h-screen bg-jc-eggshell">
      <div className="container">
        <div className="grid grid-cols-12 gap-4">
          <div className="col-span-12">
            <div className="bg-jc-white w-full mx-auto max-w-[375px] md:max-w-[736px] md:rounded-xl md:my-[61.5px] 2xl:my-[123px]">
              <div className="md:pt-10 md:px-10">
                <img src="./images/image-omelette.jpeg" alt="Omelette" />
              </div>

              <section className="pt-9 pb-[33px] px-[30px] md:px-10 md:pt-10">
                <h1 className="font-youngSerif font-normal text-4xl/none">
                  Simple Omelette Recipe
                </h1>
                <p className="mt-6 text-jc-dark-charcoal">
                  An easy and quick dish, perfect for any meal. This classic
                  omelette combines beaten eggs cooked to perfection, optionally
                  filled with your choice of cheese, vegetables, or meats.
                </p>
              </section>

              <section className="pb-9 px-[30px] md:px-10 md:pb-10">
                <div className="bg-jc-rose-white p-7 rounded-lg">
                  <p className="inline-block text-jc-dark-raspberry text-xl font-semibold">
                    Preparation time
                  </p>

                  <ul className="text-jc-wenge-brown space-y-3 mt-4">
                    {[
                      {
                        name: "Total: ",
                        text: "Approximately 10 minutes",
                      },
                      {
                        name: "Preparation: ",
                        text: "5 minutes",
                      },
                      {
                        name: "Cooking: ",
                        text: "5 minutes",
                      },
                    ].map(({ name, text }, i) => (
                      <li
                        key={i}
                        className="relative before:content-[''] before:absolute before:w-1.5 before:h-1.5 before:bg-jc-dark-raspberry before:rounded-full before:left-0 before:top-1/2 before:-translate-y-1/2"
                      >
                        <div className="ml-8 leading-tight">
                          <span className="font-semibold">{name}</span>
                          <span>{text}</span>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </section>

              <section className="pb-9 px-[30px] md:px-10 md:pb-10">
                <p className="inline-block text-jc-nutmeg text-3xl font-semibold md:text-4xl">
                  Ingredients
                </p>
                <ul className="text-jc-wenge-brown space-y-3 mt-5">
                  {[
                    {
                      text: "2-3 large eggs",
                    },
                    {
                      text: "Salt, to taste",
                    },
                    {
                      text: "Pepper, to taste",
                    },
                    {
                      text: "1 tablespoon of butter or oil",
                    },
                    {
                      text: "Optional fillings: cheese, diced vegetables, cooked meats, herbs",
                    },
                  ].map(({ text }, i) => (
                    <li
                      key={i}
                      className="relative before:content-[''] before:absolute before:w-1.5 before:h-1.5 before:bg-jc-nutmeg before:rounded-full before:left-0 before:top-1/2 before:-translate-y-1/2"
                    >
                      <div className="ml-8 leading-tight">
                        <span>{text}</span>
                      </div>
                    </li>
                  ))}
                </ul>
              </section>

              <hr className="mb-9 mx-[30px] md:mx-10 md:mb-10" />

              <section className="pb-9 px-[30px] md:px-10 md:pb-10">
                <p className="inline-block text-jc-nutmeg text-3xl font-semibold md:text-4xl">
                  Instructions
                </p>
                <ul className="text-jc-wenge-brown space-y-3 mt-5">
                  {[
                    {
                      name: "Beat the eggs: ",
                      text: "In a bowl, beat the eggs with a pinch of salt and pepper until they are well mixed. You can add a tablespoon of water or milk for a fluffier texture.",
                    },
                    {
                      name: "Heat the pan: ",
                      text: "Place a non-stick frying pan over medium heat and add butter or oil.",
                    },
                    {
                      name: "Cook the omelette: ",
                      text: "Once the butter is melted and bubbling, pour in the eggs. Tilt the pan to ensure the eggs evenly coat the surface.",
                    },
                    {
                      name: "Add fillings (optional): ",
                      text: "When the eggs begin to set at the edges but are still slightly runny in the middle, sprinkle your chosen fillings over one half of the omelette.",
                    },
                    {
                      name: "Fold and serve: ",
                      text: "As the omelette continues to cook, carefully lift one edge and fold it over the fillings. Let it cook for another minute, then slide it onto a plate.",
                    },
                    {
                      name: "Enjoy: ",
                      text: "Serve hot, with additional salt and pepper if needed.",
                    },
                  ].map(({ name, text }, i) => (
                    <li key={i} className="relative flex">
                      <span className="text-jc-nutmeg font-semibold">
                        {i + 1}.
                      </span>
                      <div className="ml-8 leading-tight">
                        <span className="font-semibold">{name}</span>
                        <span>{text}</span>
                      </div>
                    </li>
                  ))}
                </ul>
              </section>

              <hr className="mb-9 mx-[30px] md:mx-10 md:mb-10" />

              <section className="pb-9 px-[30px] md:px-10 md:pb-10">
                <p className="inline-block text-jc-nutmeg text-3xl font-semibold md:text-4xl">
                  Nutrition
                </p>
                <p className="mt-5 text-jc-wenge-brown">
                  The table below shows nutritional values per serving without
                  the additional fillings.
                </p>

                <div className="mt-5">
                  {[
                    {
                      name: "Calories",
                      amount: "277kcal",
                    },
                    {
                      name: "Carbs",
                      amount: "0g",
                    },
                    {
                      name: "Protein",
                      amount: "20g",
                    },
                    {
                      name: "Fat",
                      amount: "22g",
                    },
                  ].map(({ name, amount }, i) => (
                    <div
                      key={i}
                      className={`border-jc-nutmeg/25 flex justify-center items-center gap-x-7 py-2 md:gap-x-60 md:justify-start md:px-8 ${
                        i === 0 ? "" : "border-t"
                      }`}
                    >
                      <span className="w-16">{name}</span>
                      <span className="w-16 text-jc-nutmeg font-semibold">
                        {amount}
                      </span>
                    </div>
                  ))}
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};
