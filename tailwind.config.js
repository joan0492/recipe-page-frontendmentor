/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],

  theme: {
    /*---Start Addapter to bootstrap breakpoints---*/
    container: {
      center: true,
      /*
      padding: {
        DEFAULT: "1rem",
        sm: "calc(18px + 1rem)",
        md: "calc(24px + 1rem)",
        lg: "calc(16px + 1rem)",
        xl: "calc(30px + 1rem)",
        "2xl": "calc(40px + 1rem)",
      },
      */
      /*
      screens: {
        sm: "576px",
        md: "768px",
        lg: "992px",
        xl: "1200px",
        "2xl": "1400px",
      },
      */
    },
    /*---End Addapter to bootstrap breakpoints---*/
    extend: {
      colors: {
        // primary
        "jc-nutmeg": "hsl(14, 45%, 36%)",
        "jc-dark-raspberry": "hsl(332, 51%, 32%)",
        // neutral
        "jc-white": "hsl(0, 0%, 100%)",
        "jc-rose-white": "hsl(330, 100%, 98%)",
        "jc-eggshell": "hsl(30, 54%, 90%)",
        "jc-light-grey": "hsl(30, 18%, 87%)",
        "jc-wenge-brown": "hsl(30, 10%, 34%)",
        "jc-dark-charcoal": "hsl(24, 5%, 18%)",
      },
      fontFamily: {
        outfit: ["var(--font-outfit)"],
        youngSerif: ["var(--font-young-serif)"],
      },
    },
  },

  plugins: [],
};
